package formation.tp.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    public static final String MESSAGE_BIENVENUE = "Bonjour, bienvenue à Thymeleaf!";

    @GetMapping(value = { "/", "/index" })
    public String index(Model model) {
        model.addAttribute("message_bienvenue", MESSAGE_BIENVENUE);
        model.addAttribute("language_prog", "Java");
        return "index";
    }
}
