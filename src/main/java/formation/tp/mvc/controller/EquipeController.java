package formation.tp.mvc.controller;

import formation.tp.mvc.model.Equipe;
import formation.tp.mvc.service.EquipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class EquipeController {
    @Autowired
    private EquipeService equipeService;

    @GetMapping(value = "/equipes")
    public String listEquipes(Model model) {
        List<Equipe> equipes = equipeService.listerEquipes();
        model.addAttribute("equipes", equipes);
        return "equipes";
    }

    @GetMapping(value = "/ajouter_equipe")
    public String ajouterEquipe(Model model) {
        model.addAttribute("equipe", new Equipe());
        return "formulaire";
    }

    @PostMapping(value = "/ajouter_equipe")
    public String ajouterEquipe(@ModelAttribute Equipe equipe, BindingResult errors, Model model) {
        equipeService.ajouterEquipe(equipe);
        return "index";
    }
}
