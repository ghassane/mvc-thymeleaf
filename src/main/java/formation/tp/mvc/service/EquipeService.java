package formation.tp.mvc.service;

import formation.tp.mvc.model.Equipe;
import formation.tp.mvc.model.UserCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class EquipeService {

    private final RestTemplate restTemplate;
    private final String serverBaseURI;

    private final List<Equipe> equipes = new ArrayList<>();

    @Autowired
    public EquipeService(RestTemplate restTemplate, @Value("${uri.du.serveur.rest}") String serverBaseURI) {
        this.restTemplate = restTemplate;
        this.serverBaseURI = serverBaseURI;
    }

    /*
    public List<Equipe> listerEquipes() {
        var userCredentials = new UserCredentials();
        userCredentials.setIdentifiant("Admin");
        userCredentials.setMdp("azerty");
        String jwtToken = restTemplate.postForObject(this.serverBaseURI + "/login", userCredentials, String.class);
        if (jwtToken == null || jwtToken.length() == 0) {
            throw new IllegalStateException("JWT invalide!");
        }
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(jwtToken);
        HttpEntity<Void> entity = new HttpEntity<>( headers);
        ResponseEntity<Equipe[]> equipesResponse =
                restTemplate.exchange(this.serverBaseURI + "/equipes", HttpMethod.GET,entity, Equipe[].class);
        if(equipesResponse.getStatusCode() == HttpStatus.OK && equipesResponse.getBody() != null) {
            return Arrays.asList(equipesResponse.getBody());
        } else {
            System.out.println("Erreur http : " + equipesResponse.getStatusCodeValue());
            return Collections.emptyList();
        }
    }*/

    public List<Equipe> listerEquipes() {
        return equipes;
    }

    public void ajouterEquipe(Equipe equipe) {
        equipes.add(equipe);
    }
}
