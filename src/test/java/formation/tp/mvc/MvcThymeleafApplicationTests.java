package formation.tp.mvc;

import formation.tp.mvc.controller.IndexController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
class MvcThymeleafApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void verifier_index_est_bien_charge() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/index"))
				.andExpect(content().string(containsString(IndexController.MESSAGE_BIENVENUE)));
	}

}
